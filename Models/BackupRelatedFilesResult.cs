﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileManagementModule.Models
{
    public class BackupRelatedFilesResult
    {
        public List<ConfigResult> ConfigResults { get; set; } = new List<ConfigResult>();
    }

    public class ConfigResult
    {
        public clsOntologyItem ResultState { get; set; }
        public clsOntologyItem ConfigItem { get; set; }
        public List<clsOntologyItem> FilesSaved { get; set; } = new List<clsOntologyItem>();
    }
}
