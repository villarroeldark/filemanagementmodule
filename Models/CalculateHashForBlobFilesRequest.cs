﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace FileManagementModule.Models
{
    public class CalculateHashForBlobFilesRequest
    {
        public string IdConfig { get; private set; }
        public CancellationToken CancellationToken { get; private set; }
        public IMessageOutput MessageOutput { get; set; }

        public CalculateHashForBlobFilesRequest(string idConfig, CancellationToken cancellationToken)
        {
            IdConfig = idConfig;
            CancellationToken = cancellationToken;
        }
    }
}
