﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileManagementModule.Models
{
    public class CalculateHashForBlobFilesResult
    {
        public long FileCount { get; set; }
    }
}
