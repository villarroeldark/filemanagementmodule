﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileManagementModule.Models
{
    public class GetBackupRelatedFilesModelResult
    {
        public clsOntologyItem RootConfig { get; set; }
        public List<clsOntologyItem> ConfigItems { get; set; } = new List<clsOntologyItem>();
        public List<clsObjectAtt> RelationLevels { get; set; } = new List<clsObjectAtt>();
        public List<clsObjectRel> Paths { get; set; } = new List<clsObjectRel>();
        public List<clsObjectRel> Objects { get; set; } = new List<clsObjectRel>();
    }
}
