﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileManagementModule.Models
{
    public class GetFileAttributesModel
    {
        public clsOntologyItem Config { get; set; }
        public List<clsObjectRel> ConfigsToFileAttributes { get; set; } = new List<clsObjectRel>();
        public clsOntologyItem Path { get; set; }
        public clsOntologyItem TextParser { get; set; }
    }
}
