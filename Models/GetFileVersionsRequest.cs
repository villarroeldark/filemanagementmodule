﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileManagementModule.Models
{
    public class GetFileVersionsRequest
    {
        public string IdConfig { get; private set; }

        public GetFileVersionsRequest(string idConfig)
        {
            IdConfig = idConfig;
        }
    }
}
