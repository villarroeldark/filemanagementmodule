﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileManagementModule.Models
{
    public class GetFilesFilterForHashingResult
    {
        public List<clsOntologyItem> FilesFilter { get; set; } = new List<clsOntologyItem>();
        public bool UseFilter { get; set; }
        public bool Recalc { get; set; }
    }
}
