﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileManagementModule.Models
{
    public class GetFileversionsModelResult
    {
        public clsOntologyItem FileVersionsConfig { get; set; }
        public clsOntologyItem TextParser { get; set; }
    }
}
