﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileManagementModule.Models
{
    public class GetModelCalculateHashForBlobFilesResult
    {
        public clsOntologyItem Config { get; set; }
        public clsObjectAtt Recalculate { get; set; }
        public List<clsOntologyItem> FilesFilter { get; set; } = new List<clsOntologyItem>();
        public List<clsOntologyItem> FolderFilter { get; set; } = new List<clsOntologyItem>();
    }
}
