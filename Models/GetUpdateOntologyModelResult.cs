﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileManagementModule.Models
{
    public class GetUpdateOntologyModelResult
    {
        public List<clsOntologyItem> OntologieVersions { get; set; } = new List<clsOntologyItem>();
        public List<clsObjectAtt> OntologyVersionAttribs { get; set; } = new List<clsObjectAtt>();
        public List<clsObjectRel> OntologyVersionsToNameSpaces { get; set; } = new List<clsObjectRel>();
        public List<clsObjectRel> OntologyVersionsToPaths { get; set; } = new List<clsObjectRel>();
        public List<clsObjectRel> OntologyVersionsToOntologies { get; set; } = new List<clsObjectRel>();
    }
}
