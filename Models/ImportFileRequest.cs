﻿using ElasticSearchNestConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TextParserModule.Models;

namespace FileManagementModule.Models
{
    public class ImportFileRequest
    {
        public string Path { get; set; }
        public List<clsAppDocuments> FileDocs { get; set; } = new List<clsAppDocuments>();
        public List<clsAppDocuments> FolderDocs { get; set; } = new List<clsAppDocuments>();
        public clsUserAppDBUpdater DbWriter { get; set; }

        public TextParser TextParser { get; set; }

        public bool GetFileName { get; set; }
        public bool GetCreateTime { get; set; }
        public bool GetFileCount { get; set; }
        public bool GetFileCountRec { get; set; }
        public bool GetFileFullName { get; set; }
        public bool GetFilePath { get; set; }
        public bool GetFileSize { get; set; }

        public bool GetLastWriteTime { get; set; }

        public bool GetVersion { get; set; }

        public bool GetExtension { get; set; }

        public OntologyAppDBConnector.IMessageOutput MessageOutput { get; set; }
    }
}
