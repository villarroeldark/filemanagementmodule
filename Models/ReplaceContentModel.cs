﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileManagementModule.Models
{
    public class ReplaceContentModel
    {
        public clsOntologyItem BaseConfig { get; set; }
        public List<clsOntologyItem> Configs { get; set; } = new List<clsOntologyItem>();
        public List<clsObjectAtt> ConfigsCaseSensitive { get; set; } = new List<clsObjectAtt>();
        public List<clsObjectAtt> ConfigsFilter { get; set; } = new List<clsObjectAtt>();
        public List<clsObjectAtt> ConfigsRecursive { get; set; } = new List<clsObjectAtt>();
        public List<clsObjectAtt> ConfigsRegex { get; set; } = new List<clsObjectAtt>();
        public List<clsObjectAtt> ConfigsReplaceString { get; set; } = new List<clsObjectAtt>();
        public List<clsObjectAtt> ConfigsSearch { get; set; } = new List<clsObjectAtt>();
        public List<clsObjectAtt> ConfigsTest { get; set; } = new List<clsObjectAtt>();
        public List<clsObjectRel> ConfigsToPath { get; set; } = new List<clsObjectRel>();
        public List<clsObjectRel> ConfigsToExcludePath { get; set; } = new List<clsObjectRel>();

        public List<clsObjectAtt> ConfigsToNoLogging { get; set; } = new List<clsObjectAtt>();

        public List<clsObjectRel> ConfigsToBackupPath { get; set; } = new List<clsObjectRel>();

        public List<clsObjectRel> ConfigsToTestPath { get; set; } = new List<clsObjectRel>();

        public List<clsObjectAtt> ConfigsBackupRootPath { get; set; } = new List<clsObjectAtt>();

        public List<clsObjectRel> ConfigsToReportReplace { get; set; } = new List<clsObjectRel>();

        public List<clsObjectRel> ReportReplacesToCommandLineRuns { get; set; } = new List<clsObjectRel>();

        public List<clsObjectRel> ReportReplacesToReportFieldFilePaths { get; set; } = new List<clsObjectRel>();

        public List<clsObjectRel> ReportReplacesToReportFilters { get; set; } = new List<clsObjectRel>();
        public List<clsObjectRel> ReportReplacesToReports { get; set; } = new List<clsObjectRel>();
        public List<clsObjectRel> ReportReplacesToReportSorts { get; set; } = new List<clsObjectRel>();
        public List<clsObjectRel> ReportReplacesToVariableFields { get; set; } = new List<clsObjectRel>();

        public List<clsObjectRel> VariableFieldsToVariables { get; set; } = new List<clsObjectRel>();
        public List<clsObjectRel> VariableFieldsToReportFields { get; set; } = new List<clsObjectRel>();
    }
}
