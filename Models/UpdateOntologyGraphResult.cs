﻿using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileManagementModule.Models
{
    public class UpdateOntologyGraphResult
    {
        public List<ResultItem<OntologyVersion>> OntologyVersions { get; set; } = new List<ResultItem<OntologyVersion>>();
    }
}
